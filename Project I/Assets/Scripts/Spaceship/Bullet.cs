﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D Rigidbody2D;


        //How to 2 private AudioSource bulletAudio;
        public void Init(Vector2 direction)
        {
            Move(direction);
        }

        private void Awake()
        {
            Debug.Assert(Rigidbody2D != null, "Rigidbody2D cannot be null");
            
            /*How to 2
            bulletAudio = GetComponent<AudioSource>();
            bulletAudio.Play();*/
        }

        private void Move(Vector2 direction)
        {
            Rigidbody2D.velocity = direction * speed;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("ObjectDestroyer"))
            {
             Destroy(this.gameObject);
             return;
            }
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
        }

        
    }
}

