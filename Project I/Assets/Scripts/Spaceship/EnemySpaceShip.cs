﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;
using System;
using Manager;

namespace Spaceship
{
    public class EnemySpaceShip : BaseSpaceShip, IDamagable
    {
        public event Action OnExploded;

        public AudioClip enemyExplodeSound;
        public float enemyExplodeVolume;
        
        [SerializeField] private double fireRate = 1;
        
       
        private float fireCounter = 0;
        

        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null" );
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "Hp <= 0");
            
           //SoundManager.Instance.Play(audioSource,SoundManager.Sound.ShipCrush);
           
           AudioSource.PlayClipAtPoint(enemyExplodeSound, Camera.main.transform.position, enemyExplodeVolume);
            gameObject.SetActive(false);
            
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                SoundManager.Instance.Play(audioSource,SoundManager.Sound.EnemyFire);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }

        
    }
}

