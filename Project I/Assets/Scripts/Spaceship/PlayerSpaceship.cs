﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceShip, IDamagable
    {

        public event Action OnExploded;
        public AudioClip playerCrushSound;
        public float playerCrushSoundVolume;
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null" );
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "Hp is more than 0");
            //SoundManager.Instance.Play(audioSource,SoundManager.Sound.ShipCrush);
            AudioSource.PlayClipAtPoint(playerCrushSound, Camera.main.transform.position, playerCrushSoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        /*public void OnTriggerEnter2D(Collider2D other)
        {
            AudioSource.PlayClipAtPoint(playerCrushSound, Camera.main.transform.position, playerCrushSoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
            
        }*/
    }  
}

