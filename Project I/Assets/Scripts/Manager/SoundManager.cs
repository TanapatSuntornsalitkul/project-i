﻿using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Instance;
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;

        

        public enum Sound
        {
            BGM,
            PlayerFire,
            EnemyFire,
            ShipCrush
        }
    
        [Serializable]
        public struct SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0, 1)] public float soundVolume;
        }
    
        public void Awake()
        {
            Debug.Assert(soundClips != null && soundClips.Length != 0, "sound Clips need to be setup");
            Debug.Assert(audioSource != null, "audioSource can't be null");

            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            
            
        
        }
    
    

        public void Play(AudioSource audioSource, Sound sound)
        {
            var soundClip = GetSoundClip(sound);
            audioSource.clip = soundClip.audioClip;
            audioSource.volume = soundClip.soundVolume;
            audioSource.Play();
        }

        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource,Sound.BGM);
        }
        
        private SoundClip GetSoundClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }

            return default(SoundClip);
        }
    }
}
