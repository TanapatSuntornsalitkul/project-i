﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        [SerializeField] private Button startButton;
        [SerializeField] private Button ExitButton;
        
        
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceShip enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        public event Action OnRestarted;
       

        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        

        public PlayerSpaceship spawnedPlayerShip;

        public void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(ExitButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null,"playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceshipHp has to be more than 0");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than 0");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than 0");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than 0");
            
            
            //SoundManager.Instance.PlayBGM();  Singleton isn't work I try many times bu it still error.
            
            if (Instance == null)
            {
                Instance = this;
            }
            DontDestroyOnLoad(this);
            
            
            
           
            startButton.onClick.AddListener(OnStartButtonClicked);
            ExitButton.onClick.AddListener(OnExitButtonClicked);
            
            
            
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        

        private void OnExitButtonClicked()
        {
            Application.Quit();
            Debug.Log("Exit game");
            
            dialog.gameObject.SetActive(true);
            
        }
        
        private void StartGame()
        { 
            scoreManager.Init(this);
            
            
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayerShip = Instantiate(playerSpaceship);
            spawnedPlayerShip.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayerShip.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            //SceneManager.LoadScene(1); 
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spawnedEnemyShip = Instantiate(enemySpaceship);
            spawnedEnemyShip.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spawnedEnemyShip.OnExploded += OnEnemySpaceshipExploded;
            

            /*var enemyController = spawnedEnemyShip.GetComponent<EnemyController>();
            enemyController.Init(spawnedPlayerShip);*/

        }

        private void OnEnemySpaceshipExploded()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            //ScoreManager.Instance.SetScore(1);
            //Restart();

        }

        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            
            OnRestarted?.Invoke();

        }

        private void DestroyRemainingShips()
        {
            var remainingEnemys = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemys)
            {
                Destroy(enemy);
            }

            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }
        }
        
        
    }
}



