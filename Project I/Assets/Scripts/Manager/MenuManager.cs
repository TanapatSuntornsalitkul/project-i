﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    // Start is called before the first frame update
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }
    
    public void ExitGame()
    { 
        Application.Quit();
        Debug.Log("Exit game");
    }

    // Update is called once per frame
    
}
